(require 'ox-publish)

;; Set the package installation directory so that packages aren't stored in the
;; normal ~/.emacs.d/elpa path.
(require 'package)
(setq package-user-dir (expand-file-name "./.packages"))
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

;; Initialize the package system
(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

;; Install dependencies
(package-install 'htmlize)

;; Customize the HTML output
(setq org-html-validation-link nil            ;; Don't show validation link
      org-html-head-include-scripts nil       ;; Use our own scripts
      org-html-head-include-default-style nil ;; Use our own styles
      org-html-head "<link rel=\"stylesheet\" href=\"/static/css/site.css\" />"      
      )

(defun my-html-postamble (options)
  (concat "<hr>"
          (if (and (plist-get options ':keywords) (not (string= (plist-get options ':keywords) "")))
              (format "<p>Keywords: %s</p>" (plist-get options ':keywords))
              "")
          (format "<p class=\"date\">Modified: %s</p>" (format-time-string "%Y-%m-%d %H:%M:%S %Z"))
          (format "<p>Copyright (c) %s Tom Barron</p>"
                  (format-time-string "%Y"))))

(setq org-publish-project-alist
      '(
        ("pages"
         :base-directory "content/pages"
         :base-extension "org"
         :publishing-directory "public/"
         :recursive t
         :publishing-function org-html-publish-to-html
	 :auto-sitemap t
	 :section-numbers nil
	 :with-toc nil
	 :sitemap-title "tbarron's blog"
	 :sitemap-filename "index.org"
	 :sitemap-sort-files anti-chronologically
	 :html-link-up "/"
	 :html-link-home "/"
	 :author "Tom Barron"
	 :email "tom@barron.net"
         :html-postamble my-html-postamble)
	("static"
	 :base-directory "content/static"
 	 :base-extension "css\\|js\\|ico\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf\\|eot\\|svg\\|woff\\|woff2\\|ttf"
         :publishing-directory "./public/static"
         :publishing-function org-publish-attachment
         :recursive t)
	("all" :components ("pages" "static"))))
